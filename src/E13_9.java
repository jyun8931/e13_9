/**
 * @author John Yun
 * @email  john.yun2017@gmail.com
 * This class demonstrates the use of recursion to
 * solve a problem.
 */

public class E13_9 {
    public static void main(String[] args) {
        int index = indexOf("Missisippi", "sip");
        if(index != -1){
            System.out.printf("The index your target word starts at is %d", index);
        }
        else System.out.println("Target doens't exist in word");
    }

    /**
     * Caller method for indexOf. Starts at index 0
     * @param text
     * @param target
     * @return
     */
    public static int indexOf(String text, String target){
        return indexOfHelper(text, target, 0);
    }

    /**
     * Helper method that recursively finds the index of the
     * given word within the given string
     *
     * @param text
     * @param target
     * @param indx
     * @return
     */
    public static int indexOfHelper(String text, String target, int indx){
        if(target.length() > text.length()){
            return -1;
        }
        else if (text.substring(0, target.length()).equals(target)){
            return indx;
        }
        else{
            return indexOfHelper(text.substring(1), target, indx+1);
        }
    }
}
/**
 * Sample Output
 *
 * The index your target word starts at is 5
 * Process finished with exit code 0
 */
